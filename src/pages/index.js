import Akun from './Akun';
import DaftarMobil from './DaftarMobil';
import Home from './Home';
import SplashScreen from './SplashScreen';

export {Akun, DaftarMobil, Home, SplashScreen};
