import React, {useEffect} from 'react';
import {
  Image,
  ImageBackground,
  StyleSheet,
  View,
  StatusBar,
  Dimensions,
} from 'react-native';
import {Splash} from '../../assets';
import {colors} from '../../utils';

// props navigation didapat dari react navigation yang telah diinstal
const SplashScreen = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('MainApp');
    }, 3000);
  }, [navigation]);

  return (
    <View style={styles.page}>
      <StatusBar backgroundColor={colors.background} hidden={true} />
      <ImageBackground source={Splash} style={styles.image} />
    </View>
  );
};

export default SplashScreen;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  page: {
    flex: 1,
  },
  image: {
    width: windowWidth,
    height: windowHeight,
  },
});
