import Banner from './Banner.png';
import Allura from './Allura.png';
import Mercedes from './Mercedes.png';
import Splash from './Splash.png';
import HeaderBackground from './headerBackground.png';
import profil from './profil.png';

export {Banner, Splash, HeaderBackground, profil, Mercedes, Allura};
